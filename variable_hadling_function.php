<?php

    //floatVal()
    $myVar = "424 is the value 465.25";
    $result = floatval($myVar);
    //echo $result;
    var_dump($result);


                        //empty()
                        $myVar1 = "This is a value";
                        $myVar2= "";
                        if (empty($myVar1))
                            echo "yes,that is empty <br>";
                        else
                            echo "No,that is not empty <br>";


    //is Array()

    $myVar = array("hello","world");
    $myVar = 450;

    if (is_array($myVar))
        echo "yes,that is an array <br>";
    else
        echo "No,that is not an array <br>";


                        //is null()
                        $myVar = NULL;
                        $myVar = 450;

                        if (is_null($myVar))
                            echo "yes,that is null <br>";
                        else
                            echo "No,that is not null <br>";


    //is object()
   class MyClass{
       private $myProperty1;
   }
    $myVar = New MyClass();
    $myVar = array("hello","world");

    if (is_object($myVar))
        echo "yes,that is an object <br>";
    else
        echo "No,that is not an object <br>";



                        //isset
                        //$myVar = NULL;
                        $myVar = 450;

                        if (is_null($myVar))
                            echo "yes,that is set <br>";
                        else
                            echo "No,that is not set <br>";

                        //unset
                        $myVar = "jmjfdsjhkjhdskjhgk<br>";
                        unset($myVar);
                        echo $myVar;


    //var_export

    $myVar = array("hello","world");
    var_export($myVar);
    $myAnotherVar = array ( 0 => 'hello', 1 => 'world', );
    print_r($myAnotherVar);
    echo "<br>";


    //boolVal

    echo '0:        '.(boolval(0) ? 'true' : 'false')."\n";
    echo '42:       '.(boolval(42) ? 'true' : 'false')."\n";
    echo '0.0:      '.(boolval(0.0) ? 'true' : 'false')."\n";
    echo '4.2:      '.(boolval(4.2) ? 'true' : 'false')."\n";
    echo '"":       '.(boolval("") ? 'true' : 'false')."\n";
    echo '"string": '.(boolval("string") ? 'true' : 'false')."\n";
    echo '"0":      '.(boolval("0") ? 'true' : 'false')."\n";
    echo '"1":      '.(boolval("1") ? 'true' : 'false')."\n";
    echo '[1, 2]:   '.(boolval([1, 2]) ? 'true' : 'false')."\n";
    echo '[]:       '.(boolval([]) ? 'true' : 'false')."\n";
    echo 'stdClass: '.(boolval(new stdClass) ? 'true' : 'false')."\n";


